function expecVal = calcExpecVal(x,psiList)
    global nt;
    
    expecVal = zeros(1,nt);
    
    for i = 1:nt
        psi_i = psiList(:,i);
        val = overlap(psi_i,x.*psi_i);
        expecVal(1,i) = val;
    end
end
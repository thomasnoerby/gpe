function H = makeHamiltonian(u)
V = diag(makePotential(u));
T = makeKinetic();
H = T+V;
end
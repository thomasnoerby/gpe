% run main until gradient test

psi = psiInit;
% create MATLAB FList
FList = zeros(nt,1);
FList(1,:) = fidelity(psi,psiTar);
for i = 1:nt-1
    ui = u(i:i+1);
    psi = forwardPropagator(ui,psi);
    FList(i+1,:) = fidelity(psi,psiTar);
end


% load QEngine results
load ../../QEngineResults/anGrad1.mat;

% Fidelity trace plot
figure()
title("Fidelity trace, unoptimized control, $$g=1$$")
hold on
xlabel("Index")
ylabel("Fidelity")
legend show;
xlim([0,101])

plot(FList,'DisplayName','MATLAB')
plot(fidelity_unopt,'--','DisplayName','QEngine')

set(gcf,'Position',[382 49 1176 836])


% Fidelity trace ratio plot
figure()
title("Ratio of fidelity trace, unoptimized control")
hold on
xlabel("Iteration")
ylabel("$$1-F_{MATLAB}/F_{QEngine}$$")

% plot(1-FList./fidelity_unopt)
plot(1-fidelity_unopt./FList)

set(gcf,'Position',[382 49 1176 836])


% State comparison
figure()
plot((conj(psi0).*psi0)./(conj(psi_0).*psi_0))
set(gcf,'Position',[382 49 1176 836])

psiSqMat = conj(psiInit).*psiInit;
psiSqAn = conj(psi_0).*psi_0;

load ../../QEngineResults/numGrad1.mat;

psiSqNum = conj(psi_0).*psi_0;

figure()
plot(psiSqMat,'k-','DisplayName','MATLAB')
hold on
title("$$|\psi|^2$$ comparison, $$g=1$$")
plot(psiSqAn,'r--','DisplayName','QEngine')
xlabel("Index")
ylabel("$$|\psi|^2$$")
set(gcf,'Position',[382 49 1176 836])
legend show
xlim([0, 512])

figure()
% plot(1-psiSqMat(1:end-100)./psiSqNum(1:end-100))
plot(psiSqNum-psiSqMat)
hold on
ylabel("$$|\psi|^2_{MAT}-|\psi|^2_{QEn}$$")
title("$$|\psi|^2$$ difference, $$g=1$$")
set(gcf,'Position',[382 49 1176 836])
xlim([0,512])
xlabel("Index")

function [eigVec, eigVal] = diagonalizeGpe(u,psi)
    H = makeGpeHamiltonian(u,psi);
    [eigVec, eigVal] = eig(H);
end
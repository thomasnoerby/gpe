function cost = costFunction(u)
    global psiTar gamma contrFuncDerInt;
    
    F = fidelity(psiTar,forwardPropagationFinal(u));    
    
%     cost = 1/2*(1-F)+gamma/2*contrFuncDerInt;
    cost = 0.5*(1-F);
end
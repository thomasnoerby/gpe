% function psi = splitStepForwardPropagation(u,psi,x)
%     global dt kvec nx;
% 
%     uFrom = u(1);
%     uTo = u(2);
%     
%     VFrom = makePotential(uFrom); % potential
%     VTo = makePotential(uTo); % potential
%     
%     UvFrom = exp(-1i*VFrom'*dt/2); % ' is the transpose conjugate - V is real however, so it is okay
%     UvTo = exp(-1i*VTo'*dt/2);
%     
%     Ut = exp(-1i*kvec.^2/2*dt); % kinetic term
%     
%     psi = UvFrom.*psi;
%     psi = fft(psi);
%     psi = Ut.*psi;
%     psi = ifft(psi);
%     psi = UvTo.*psi;
%     
%     VGpeFrom = makeGpePotential(uFrom,psi);
%     UvFrom = exp(-1i*VGpeFrom'*dt/2);
%     psiP = UvFrom.*psi;
%     
%     psiPP = zeros(size(psi));
%     for k = 1:nx
%         expVal = 1i*kvec(k)*x;
%         eK = exp(-expVal);
%         psiK = psiP.*eK;
%         psiK = psiK/nx;
%         
%         eKplus = exp(expVal);
%         expVal2 = exp(-1i*kvec(k)^2*dt/2);
%         psiPP = psiPP+psiK*eKplus*expVal2;
%     end
%     
%     VGpeTo = makeGpePotential(uTo,psiPP);
%     psi = exp(-1i*VGpeTo'*dt/2).*psiPP;
% end
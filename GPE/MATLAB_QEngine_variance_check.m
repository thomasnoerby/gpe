% purge;
close all;
global g;
% load("gList.mat");
% load("var0ListVariation.mat");
% load("var1List.mat");
load("../../QEngineResults/variance.mat")
gList = g;
g = gList(1);

% psiList = psi_1;
% psi = psiList(:,1);
% V = makeGpePotential(u0,psi);

% figure()
% f = plot(x,psi(:,1).*conj(psi(:,1)));
% hold on
% axis([-3 3 0 1.8])
% xlim([-3 3])
% yyaxis right
% h = plot(x,V);
% axis([-3 3 -130 0])

% var1 = [];
% var2 = [];

psiListM = zeros(512,1000);

for i = 1:1000
    g = gList(i);
%     psi_i = psiList(:,i);
    
    [psiListMatlab,~] = groundStateBEC(u0,psi1);
    psiListM(:,i) = psiListMatlab;
%     V = makeGpePotential(u0,psi_i);
%     Vmin = min(V);
%     HQ = makeGpeHamiltonian(u0,psi_i);
%     HM = makeGpeHamiltonian(u0,psiMatlab);
%     title(num2str(g));
    
    
%     set(f,'YData',psi_i.*conj(psi_i)*60+Vmin);
%     set(h,'YData',V);
    
%     var1 = [var1, variance(psi_i,HQ)];
%     var2 = [var2, variance(psiMatlab,HM)];
    
%     pause(0.01)
    i
end
save("psiListMatlab.mat","psiListM");
% figure()
% plot(var1);

% figure()
% plot(var2)

%% after runnning: save the variance matrices, run the above again and save all the psi_1's. Plot it together with the QEngine results.

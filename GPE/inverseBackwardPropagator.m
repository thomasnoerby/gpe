function chi = inverseBackwardPropagator(u,chi,psi)
    global dt kVec;
    
    uFrom = u(1);
    uTo = u(2);
    
    chi = chiSplitStepInverse(chi,psi,uFrom);
    
    chi = fft(chi);
    
    Ut = exp(-1i*kVec.^2/2*dt);
    chi = Ut.*chi;
    
    chi = ifft(chi);
    
    chi = chiSplitStepInverse(chi,psi,uTo);
end
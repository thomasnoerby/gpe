function [psiList,psipList,psippList] = inverseForwardPropagationPsi(u)
    global nt psiTar;

    psi = psiTar;
    [n,~] = size(psi);
    
    psiList = zeros(n,nt);
    psiList(:,end) = psi;
    
    psipList = zeros(n,nt);
    psippList = zeros(n,nt);
    
    for i = nt-1:-1:1
        ui = fliplr(u(i:i+1));
        [psi,psip,psipp] = inverseForwardPropagator(ui,psi);
        psiList(:,i) = psi;
        psippList(:,i) = psipp;
        psipList(:,i) = psip;
    end
end
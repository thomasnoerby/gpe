function varCheck = variance(psi,H)
    Hpsi = H*psi;
    HHpsi = H*Hpsi;
    varCheck = overlap(psi,HHpsi)-overlap(psi,Hpsi)^2;
end
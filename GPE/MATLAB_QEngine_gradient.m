% run main until gradient test

load ../../QEngineResults/numGrad0.mat;
figure(1);
hold on
plot(t,-gradient_num(:,1),'DisplayName','Numerical Gradient (QEngine)')
title("Gradient comparison, $$g=0$$")

load ../../QEngineResults/anGrad0.mat;
plot(t,-gradient_an(:,1),'--','DisplayName','Analytical Gradient (QEngine)')

set(gcf,'Position',[382 49 1176 836])

yyaxis right
load("numGrad.mat")
% figure()
plot(t',1-numGrad'./(-gradient_num(:,1)),'DisplayName','Num. grad. comp. (right axis)')
hold on
% xlabel("Index")
ylabel("$$1-grad_{MAT}/grad_{QEn}$$")
% title("Numerical gradient comparison")
axis([0 1 -0.5 0.5])
function [f,grad] = bfgsFuncWithGrad(u)
    
    global psiTar;
    f = -fidelity(psiTar,forwardPropagationFinal(u));
%     grad = analyticalGradientCost(u)*1e6;
    grad = analyticalGradientCost(u);
%     grad = numericalGradientCost(u);
end
function [psiList,FList] = forwardPropagationFList(u)
    % forwardPropagation but returns fidelity at each
    % step as well instead of ppsiList and pppsiList
    global psiInit nt psiTar;
    
    [n,~] = size(psiInit);
    
    psi = psiInit;
    psiList = zeros(n,nt);
    psiList(:,1) = psi;
    FList = zeros(1,nt);
    FList(1,1) = fidelity(psiTar,psi);
    
    for i = 1:nt-1
        ui = u(i:i+1);
        [psi,~,~] = forwardPropagator(ui,psi);
        psiList(:,i+1) = psi;
        FList(1,i+1) = fidelity(psiTar,psi);
    end
end
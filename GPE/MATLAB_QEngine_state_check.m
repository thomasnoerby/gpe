% purge;
close all;
global g;
load("psiListMatlab.mat");
psiListM = psiListM;
load("../../QEngineResults/variance.mat")
psiListQ = psi_1;
gList = g;


% psiList = psi_1;
psiQ = psiListQ(:,1);
psiM = psiListM(:,1);
% V = makeGpePotential(u0,psi);

figure()
Q = plot(psiQ(:,1).*conj(psiQ(:,1)));
hold on
M = plot(psiM(:,1).*conj(psiM(:,1)),'--');
% axis([-3 3 0 1.8])
% xlim([-3 3])
% yyaxis right
% h = plot(x,V);
% axis([-3 3 -130 0])

% var1 = [];
% var2 = [];

% psiList = zeros(512,1000);
% figure()
varM = [];
varQ = [];
varQM = [];
figure()
s = plot(psiQ-psiM);
for i = 1:1000
    psiQ_i = psiListQ(:,i);
    psiM_i = psiListM(:,i);
    
    title(num2str(g));
    
    
    set(M,'YData',psiM_i.*conj(psiM_i));
    set(Q,'YData',psiQ_i.*conj(psiQ_i));
    
    g = gList(i);
    HQ = makeGpeHamiltonian(-1,psiQ_i);
    HM = makeGpeHamiltonian(-1,psiM_i);
    
    varQ = [varQ, variance(psiQ_i,HQ)];
    varM = [varM, variance(psiM_i,HM)];
    varQM = [varQM, variance(psiQ_i,HM)];
    
    set(s,'YData',psiQ_i-psiM_i);
    pause(0.01)
%     plot(i,varQ,'b')
%     hold on
%     plot(i,varM,'r')
end
figure()
plot(varQ)

figure()
plot(varM)

figure()
plot(varQM)

% figure()
% plot(var1);

% figure()
% plot(var2)

%% after runnning: save the variance matrices, run the above again and save all the psi_1's. Plot it together with the QEngine results.

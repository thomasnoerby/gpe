function [cost,grad] = bfgsFuncWithGradCost(u)
    
    cost = costFunction(u);
    grad = analyticalGradientCost(u);
%     grad = numericalGradientCost(u);
end
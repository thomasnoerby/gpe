clear all; close all; clc;

global nx dx x nt dt t vDepth sigma kVec UtBackward UtForward psiTar psiInit g gamma contrFuncDerInt outputVals ratioTest;
outputVals = [];
ratioTest = [];

% global p2 p4 p6;
% p2 = 65.8392;
% p4 = 97.6349;
% p6 = -15.3850;

tic
% time definitions
t0 = 0; % initial time
T = 1; % final time
dt = 1e-2; % length of a time step
nt = floor(T/dt)+1; % number of time slices
t = linspace(t0, T, nt); % time interval

% spatial definitions
% xMin = -3;
% xMax = 3;
xMin = -3;
xMax = 3;
L = xMax-xMin;
nx = 256; % number of x-steps
x = linspace(xMin, xMax, nx)'; % x interval

dx = (x(2)-x(1)); % length of an x-step

% momentum definitions
kVec = 2*pi/L*[0:nx/2-1, -nx/2:-1]';
UtForward = exp(-1i*kVec.^2/2*dt);
UtBackward = exp(+1i*kVec.^2/2*dt);
% BEC definitions
g = 8.3; % interaction parameter

% initial potential and control function positions
A = -1; % position of initial potential
B = 1; % position of target potential
u0 = A; % initial control function position
uTar = B;

% control function
% u = A+(1-A/uTar)*sin(t/T*pi/2); 
u = 1./(1+exp(-(t-T/2)*10))*2-1;
% u = 0.55*sin(pi/T*t);
% u = 0*t;
% u = ones(size(t));
% u = [linspace(0,-1,floor(nt/4)),linspace(-1,0,floor(nt/4)),linspace(0,1,floor(nt/4)),linspace(1,0,floor(nt/4)+1)];

% control function derivate 
contrFuncDer = @(ts) (1-A/uTar)*pi/T/2*cos(ts/T*pi/2);
% contrFuncDer = @(ts) 0*ts;

contrFuncDerSqr = @(ts) contrFuncDer(ts).^2;
contrFuncDerInt = integral(contrFuncDerSqr,0,T);

% % control function plot
% uPlot = 1;
% figure(uPlot)
% plot(t,u,'--')
% hold on

% initial state
vDepth = 130; % depth of potential
sigma = 1; % width of potential

% single particle initial
V = makePotential(u0); % create potential (only for plotting)
[eigVec, ~] = diagonalize(u0); % find eigenvectors of the single particle system
eigVec = eigVec/sqrt(dx); % proper normalization
psi0 = eigVec(:,1); % ground state
psi1 = eigVec(:,2); % first excited state
psi = psi0; % chosen state

Vtar = makePotential(uTar);
[eigVecTar, ~] = diagonalize(uTar);
eigVecTar = eigVecTar/sqrt(dx);
psi0Tar = eigVecTar(:,1);
psi1Tar = eigVecTar(:,2);
psiTar = psi0Tar;

% calculating BEC ground states and potentials
disp('Calculating ground state')
tic
[psiInit,E] = groundStateBEC(u0,psi);
% return;
[psiTar,Etar] = groundStateBEC(uTar,psiTar);
toc

psi = psiInit;

% VGpe = makeGpePotential(u0,psiInit);
% VGpeTar = makeGpePotential(uTar,psiTar);

% Cost function parameters
gamma = 1e-4;



% tests
% checkPropagation = testPropagation(u);
% assert(checkPropagation == 1);
% gradRatio = testGradient(u);
% return;
% 
% 
% % return;


% Plot
fig = figure();
leftColor = [1 0 0];
rightColor = [0 0 0];
set(fig,'defaultAxesColorOrder',[leftColor; rightColor]);
hold on
xlabel("$$x$$ [sim. units]")
ylabel("Wavefunc. dens. [sim. units]")
titleStr = strcat("Ground state, ","$$g$$ = ",num2str(g));
title(titleStr);

axis([-3 3 0 2])

scale = 1;
f = plot(x,conj(psi).*psi*scale,'k-','DisplayName',"$$\psi$$"); % psi plot
plot(x,conj(psiTar).*psiTar*scale,'k--','DisplayName',"$$\psi_{tar}$$")
legend show



% plotting potential on figure
yyaxis right;

ylabel("Potential [sim. units]")
h = plot(x,V,'DisplayName',"$$V$$"); % V plot
plot(x,Vtar,'DisplayName',"$$V_{tar}$$")
% axis([-3 3 -130 0])

set(gcf,"Position",[387 49 1166 843])

for i = 1:nt-1
    ui = u(i:i+1);
    psi = forwardPropagator(ui,psi);
    
%     v = makeGpePotential(u(i+1),psi);
    v = makePotential(u(i));
    
    set(f,'YData',(psi.*conj(psi))*scale);
    set(h,'YData',v);
    
    pause(0.01)
end
% return;
% %% optimize
% 
F = fidelity(psi,psiTar)
% return;
% % %
disp('Launching BFGS')
% tic
% return;
[cost,u] = bfgs(u);
% toc
% figure(uPlot)
% plot(t,u)
% return;
% return;
%%
psi = psiInit;
for i = 1:nt-1
    ui = u(i:i+1);
    psi = forwardPropagator(ui,psi);
    
%     v = makeGpePotential(u(i+1),psi);
    v = makePotential(u(i));
    
    set(f,'YData',(psi.*conj(psi))*scale);
    set(h,'YData',v)
    
    pause(0.01)
end
% toc
F = fidelity(psi,psiTar)

return;
%% optimization runs
disp("Initializing optimization")
tic

nSeeds = 50;
nT = 11;
Tmin = 1;
Tmax = 2;

Ts = linspace(Tmin,Tmax,nT);

% uFunc = @(T,tArray) A+(1-A/B)*sin(tArray/T*pi/2);
uFunc = @(nt) [linspace(0,-1,floor(nt/4)),linspace(-1,0,floor(nt/4)-floor(nt/4)*4-1+nt),linspace(0,1,floor(nt/4)),linspace(1,0,floor(nt/4)+1)];
scale = 1;

F0 = 0;
Fs = zeros(nSeeds,nT);

[runNr,~] = size(dir("Runs"));
% runNr = runNr-1;
runNr = 2;
folderName = strcat("Runs/run",num2str(runNr));
mkdir(folderName)

u0 = [];

save(strcat(folderName,"/g.mat"),"g");
save(strcat(folderName,"/Ts.mat"),"Ts");
save(strcat(folderName,"/nSeeds.mat"),"nSeeds");
save(strcat(folderName,"/psiInit.mat"),"psiInit");
save(strcat(folderName,"/psiTar.mat"),"psiTar");
save(strcat(folderName,"/u0.mat"),"u0");
save(strcat(folderName,"/uTar.mat"),"uTar");

for m = 11:nT
    disp(strcat("Initializing T ",num2str(m),"/",num2str(nT)))
    T = Ts(1,m);
    nt = floor(T/dt)+1;
    tArray = linspace(t0,T,nt);
   
%     u0 = uFunc(T,tArray);
    u0 = uFunc(nt);
    
    newFolderName = strcat(folderName,"/T_",num2str(m));
    mkdir(newFolderName)
    
    for j = 17:nSeeds
        r = randn(size(u0))*scale;
        u = u0+r;
        
        [cost,uOpt] = bfgs(u);
        saveData(newFolderName,T,cost,u,uOpt,scale)
        
        Fs(j,m) = cost;
    end
end



toc
%%
% F(T) curve
FTplot = figure();
semilogy(Ts,1-Fs,'k.')
hold on
semilogy(Ts,1-max(Fs),'r.')
semilogy([Tmin Tmax],[1e-2 1e-2],'k--')
axis([Tmin*0.98 Tmax*1.02 0.008 1.1])
xlabel("$$t$$")
ylabel("$$1-F$$")
saveName = strcat(folderName,"/FTplot");
savefig(FTplot,saveName)
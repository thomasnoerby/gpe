function chi = chiSplitStep(chi,psi,u)
%     z = overlap(chi,chi);
    
    global dt g ratioTest;
    chiRe = real(chi);
    chiIm = imag(chi);

    
    V = makePotential(u);
    
%     A = V+2*g*psi.*conj(psi);
    A = V+2*g*abs(psi).*abs(psi);
    
    a = g*psi.*psi;
    aRe = real(a);
    aIm = imag(a);


    uVecNorm = sqrt(aRe.*aRe+aIm.*aIm+A.*A);
    c = cos(-dt/2*uVecNorm);
    s = sin(-dt/2*uVecNorm)./uVecNorm;
    
    chiRe = c.*chiRe + s.*(aIm.*chiRe + (A-aRe).*chiIm);
    chiIm = c.*chiIm - s.*((A+aRe).*chiRe + aIm.*chiIm);

    chi = chiRe+1i*chiIm;
%     z2 = overlap(chi,chi);
%     ratioTest = [ratioTest, z/z2];
%     chi = chi;
    
%     disp(strcat("Current norm: ",num2str(z2),". Current ratio: ", num2str(z/z2)))
end
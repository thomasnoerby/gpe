function H = makeGpeHamiltonian(u,psi)
    VGpe = diag(makeGpePotential(u,psi));
    T = makeKinetic();
    H = T+VGpe;
end
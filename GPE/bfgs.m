function [F,u] = bfgs(u0)
    global outputVals;

    Fthresh = 0.999;
    maxFuncEval = 1500;
    maxIter = 10;
    
    opt = optimoptions(@fminunc,'Display','off','ObjectiveLimit',-Fthresh,...
        'MaxFunctionEvaluations',maxFuncEval, ...
        'MaxIterations',maxIter);
    
    % setting problem
    problem.options = opt;
    problem.x0 = u0;
    problem.objective = @bfgsFuncWithGrad;
    problem.solver = 'fminunc';
    
    [u,F,~,output] = fminunc(problem);
    outs = [output.funcCount;output.iterations];
    outputVals = [outputVals,outs];
    
    F = -F;
       
end
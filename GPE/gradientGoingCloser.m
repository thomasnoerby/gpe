purge;

anGrad1 = load("anGrad1.mat").anGrad;
anGrad2 = load("anGrad2.mat").anGrad;
anGrad3 = load("anGrad3.mat").anGrad;
anGrad4 = load("anGrad4.mat").anGrad;

numGrad1 = load("numGrad1.mat").numGrad;
numGrad2 = load("numGrad2.mat").numGrad;
numGrad3 = load("numGrad3.mat").numGrad;

x1 = linspace(0,1,11);
x2 = linspace(0,1,101);
x3 = linspace(0,1,1001);
x4 = linspace(0,1,10001);

figure()
xlabel("$$t$$")
hold on
% plot(x1,-numGrad1)
% plot(x2,numGrad2*10)
plot(x3,numGrad3*100,'-')

% plot(x1,-fliplr(anGrad1),'-.')
plot(x2,anGrad2*10,'-.')
plot(x3,anGrad3*1e2,'-.')
plot(x4,anGrad4*1e3,'-.')
function ratio = testGradient(u)
    global t;

    anGrad = analyticalGradientCost(u)';
%     anGrad = analyticalGradientOld(u)';
    numGrad = numericalGradientCost(u);
    ratio = numGrad./anGrad;
    
    % plot
    figure();
    xlabel("$$t$$");
    hold on
    ylabel("Gradient");
    
    save("numGrad.mat","numGrad")
    
    
    plot(t,numGrad,'ro','DisplayName','Numerical Gradient (MATLAB)');
    plot(t,anGrad,'b.-','DisplayName','Analytical Gradient (MATLAB) ');
    
    legend show;
    
    figure();
    ylabel("1-anGrad/numGrad");
    hold on
    xlabel("Index of gradients");
    
    semilogy(1-anGrad./numGrad,'.')
    
end
function chiList = backwardPropagation(u,psiList,psippList)
    global psiTar nt;
    
    [n,~] = size(psiTar);
    
%     psiT = psiList(:,end);
%     psiT = psiList2(:,end);
%     o = -1i*overlap(psiT,psiTar);
    
%     chi = psiTar*o;
    chi = psiTar;
    chiList = zeros(n,nt);
    chiList(:,end) = chi;
       
    for j = nt-1:-1:1
        ui = fliplr(u(j:j+1));
%         psiToFrom = fliplr(psiList(:,i:i+1));
        psi = psiList(:,j);
        psipp = psippList(:,j);
        psiToFrom = fliplr([psi,psipp]); % should perhaps be flipped
        chi = backwardPropagator(ui,chi,psiToFrom);
        chiList(:,j) = chi;
    end
end
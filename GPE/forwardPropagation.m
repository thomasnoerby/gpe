function [psiList,psipList,psippList] = forwardPropagation(u)
    global psiInit nt;
    
    [n,~] = size(psiInit);
    
    psi = psiInit;
    psiList = zeros(n,nt);
    psiList(:,1) = psi;
    
    psipList = zeros(n,nt);
    psippList = zeros(n,nt);
    
    for i = 1:nt-1
        ui = u(i:i+1);
        [psi,psip,psipp] = forwardPropagator(ui,psi);
        psiList(:,i+1) = psi;
        psippList(:,i+1) = psipp;
        psipList(:,i+1) = psip;
    end
end
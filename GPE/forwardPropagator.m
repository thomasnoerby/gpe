function [psi,psip,psipp] = forwardPropagator(u,psi)
    global dt UtForward;
    
    uFrom = u(1);
    uTo = u(2);
    
    VGpeFrom = makeGpePotential(uFrom,psi);
    
    UvFrom = exp(-1i*VGpeFrom*dt/2);
    
    psip = UvFrom.*psi;
    psi = fft(psip);
    psi = UtForward.*psi;
    psipp = ifft(psi);
    
    VGpeTo = makeGpePotential(uTo,psipp);
    
    UvTo = exp(-1i*VGpeTo*dt/2);
    
    psi = UvTo.*psipp;   
end
function V = makePotential(u)
    global x sigma vDepth p2 p4 p6;
    V = -vDepth*exp(-(x-u).^2/(2*sigma^2));
    
%     x_u = x-u;
%     x_uPow2 = x_u.*x_u;
%     x_uPow4 = x_uPow2.*x_uPow2;
%     x_uPow6 = x_uPow2.*x_uPow4;
%     
%     V = p2.*x_uPow2 + p4.*x_uPow4+p6.*x_uPow6;
end
function grad = analyticalGradientCost(u)
    global nt dt psiTar gamma dx;

    [psiList,psipList,psippList] = forwardPropagation(u);
    [psiListReverse,psipListReverse,psippListReverse] = inverseForwardPropagationPsi(u);
    chiList = backwardPropagation(u,psiListReverse,psippListReverse); 
%     chiList = backwardPropagation(u,psiList);
    
    grad = zeros(nt,1);
    psiT = psiList(:,end);
    o = -1i*overlap(psiT,psiTar);

    
    for j = 1:nt
        % V grad
        uj = u(j);
        chi = chiList(:,j);
        psi = psiList(:,j);
        
        gradV = analyticalPotentialGradient(uj);
        
        expecVal = overlap(chi,gradV.*psi); % this
%         expecVal = overlap(psi,gradV.*chi);
%         grad_j = real(-2*dt*o*expecVal);
        grad_j = real(dt*o*expecVal); % this
%         grad_j = -dt*real(o*expecVal);
%         grad_j = -real(dt*expecVal);
        
        % wiggle in cost
        wiggle = 0;
        if(j~=1 && j~=nt)
            u_jm = u(j-1);
            u_j = u(j);
            u_jp = u(j+1);
            wiggle = -gamma*(u_jp-2*u_j+u_jm)/(dt^2);
        end

        % total function
        grad(j,1) = grad_j;
    end
    grad(1,1) = 0.5*grad(1,1);
    grad(end,1) = 0.5*grad(end,1);
end
function saveData(folderName,T,cost,u0,uOpt,scale)
    mainFolder = pwd;
    
    cd(folderName)
    [n, ~] = size(dir);
    next = n-1;
    
    folder = strcat("seed",num2str(next));
    mkdir(folder);
    cd(folder);
    
    % saving
    save("u0.mat","u0");
    save("uOpt.mat","uOpt");
    save("cost.mat","cost");
    save("scale.mat","scale");
    save("T.mat","T");

    cd(mainFolder);
end
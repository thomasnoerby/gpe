function grad = numericalGradient(u)
    global nt psiTar;
    
%     eps_diff = eps^(1/3);
    eps_diff = eps^(1/2);
    
    ej = zeros(1,nt);
    grad = zeros(1,nt);
    
    for i = 1:nt
        ej(1,i) = eps_diff;
        
        Fplus = fidelity(psiTar,forwardPropagationFinal(u+ej));
        Fminus = fidelity(psiTar,forwardPropagationFinal(u-ej));
        
        Fdiff = Fplus-Fminus;
        
        grad(1,i) = -Fdiff/(2*eps_diff);
        
        ej(1,i) = 0;
    end
end
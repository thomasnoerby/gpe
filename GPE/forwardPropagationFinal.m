function psi = forwardPropagationFinal(u)
    global nt psiInit;
    
    psi = psiInit;
    for i = 1:nt-1
        ui = u(i:i+1);
        psi = forwardPropagator(ui,psi);
    end
end
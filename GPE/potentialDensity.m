function VDensity = potentialDensity(u,psiList)
    global nx nt;
    VDensity = zeros(nx,nt);
    for i = 1:nt
        psi_i = psiList(:,i);
        u_i = u(i);
        V_i = makeGpePotential(u_i,psi_i);
        VDensity(:,i) = V_i;
    end
end
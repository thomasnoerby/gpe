function check = testPropagation(u)
    global nt;
    
    disp('Testing propagation methods')
    
    check = 1;
    checkPsi = 1;
    checkChi = 1;
    maxDiff = eps^(1/3);
    
    psiList = forwardPropagation(u);
    psiT = psiList(:,end);
    psiListReverse = inverseForwardPropagationPsi(u,psiT);
    
    chiList = backwardPropagation(u,psiList);
    chi0 = chiList(:,1);
    chiListReverse = inverseBackwardPropagationChi(u,chi0,psiList);
    
    
    for j = 1:nt
        psiForward = psiList(:,j);
        psiBackward = psiListReverse(:,j);
        Fpsi = fidelity(psiForward,psiBackward);
        
        chiBackward = chiList(:,j);
        chiForward = chiListReverse(:,j);
        Fchi = fidelity(chiBackward,chiForward);
        
        checkPsi = abs(Fpsi-1)<maxDiff;
        checkChi = abs(Fchi-1)<maxDiff;
        if(checkPsi == 0 || checkChi == 0)
            disp('Test failed')
            check = (checkChi && checkPsi);
            if(checkPsi == 0 && checkChi == 0)
                disp('Both psi and chi propagation failed')
            elseif(checkPsi == 0)
                disp('Psi propagation failed')
            else
                disp('Chi propagation failed')
            end
            return;
        end
    end
    
    disp('Test successful')
end
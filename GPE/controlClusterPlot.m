function controlClusterPlot(runNr)
    % folders
    mainFolder = pwd; 
    
    runFolder = strcat(pwd,"/Runs/run",num2str(runNr));
    cd(runFolder);
    
    nSeeds = load("nSeeds.mat").nSeeds;
    Ts = load("Ts.mat").Ts;
    [~,nT] = size(Ts);
    
    TsMax = max(Ts); % find max T for stretching u plot
    
    % plot preparation
    Fmin = [0,0.8,0.9,0.95,0.98,0.99]; % Fidelity cut-offs
    [~,nFmin] = size(Fmin);
    
    figure()
    ylabel("Control function")
    hold on
    
    subplot(2,3,1)
    title("All control functions")
    
    for i = 1:nFmin % titles for subplots
        titleStr = strcat(num2str(Fmin(i)),"$<$F");
        if i<nFmin
            titleStr = strcat(titleStr,'$<$',num2str(Fmin(i+1)));
        else
            titleStr = strcat('F$>$',num2str(Fmin(i)));
        end
        subplot(2,3,i)
        hold on
        title(titleStr)
    end
    
    subplot(2,3,1)
    ylabel("Control function")
    subplot(2,3,4)
    ylabel("Control function")
    xlabel("$$t$$")
    subplot(2,3,5)
    xlabel("$$t$$")
    subplot(2,3,6)
    xlabel("$$t$$")
    
    % plot
    Fmax = 0;
    uOptMax = [];
    TValue_Fmax = 0;
    seed_Fmax = 0;
    
    for i = 1:nT
        Tfolder = strcat("T_",num2str(i)); 
        
        for j = 1:nSeeds
            seedFolder = strcat(Tfolder,"/seed",num2str(j));
            cd(seedFolder)
            
            u0 = load("u0.mat").u0;
            uOpt = load("uOpt.mat").uOpt;
            F = load("cost.mat").cost;
            
            if(F>Fmax)
                Fmax = F;
                uOptMax = uOpt;
                TValue_Fmax = Ts(i);
                TNum_Fmax = i;
                seed_Fmax = j;
            end
            
            Findex = nFmin;
            if(F<=0.99)
                Findex = find(F<Fmin,1);
            end
            
            [~,nU] = size(uOpt);
            xNew = linspace(0,TsMax,nU); % to stretch uOpt so all end at the same point
            
            subplot(2,3,Findex)
            hold on
            plot(xNew,uOpt,'-')
            
            cd(runFolder);
        end
    end
    
    % Best solution
    [~,nUmax] = size(uOptMax);
    x = linspace(0,TValue_Fmax,nUmax);
    
    figure()
    title(strcat("Best solution, $$F$$=",num2str(Fmax)))
    hold on
    plot(x,uOptMax)
    xlabel("$$t$$")
    ylabel("Control function")
    
    bestSolStr = strcat("Run: ",num2str(runNr),", T:",num2str(TNum_Fmax),", seed: ",num2str(seed_Fmax));
    disp("Best solution found at:")
    disp(bestSolStr)
    
    cd(mainFolder);
end
function chi = chiSplitStepInverse(chi,psi,u)
    global dt g;
    
    chiRe = real(chi);
    chiIm = imag(chi);

    a = g*psi.*psi;
    aRe = real(a);
    aIm = imag(a);

    V = makePotential(u);

    A = V+2*g*psi.*conj(psi);

    uVecNorm = sqrt(aRe'*aRe+aIm'*aIm+A'*A);
    
    cosFactor = cos(dt/2*uVecNorm);
    sinFactor = sin(-dt/2*uVecNorm)/uVecNorm;
    chiRe = cosFactor+sinFactor*(aIm.*chiRe+(A-aRe).*chiIm);
    chiIm = cosFactor+sinFactor*((-A-aRe).*chiRe-aIm.*chiIm);

    chi = chiRe+1i*chiIm;
end
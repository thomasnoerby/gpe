function psi = forwardPropagatorGroundStateBEC(u,psi)
    global dt kVec;
        
    VGpe = makeGpePotential(u,psi);
    
    Uv = exp(-VGpe*dt/2);
    Ut = exp(-kVec.^2/2*dt);
    
    psi = Uv.*psi;
    psi = fft(psi);
    psi = Ut.*psi;
    psi = ifft(psi);
    
%     VGpe = makeGpePotential(u,psi);
%     Uv = exp(-VGpe*dt/2);
    psi = Uv.*psi;  
end

global g;
n = 1001;

psin0 = psi0;
psin1 = psi1;
gList = zeros(1,n);
var0List = gList;
var1List = gList;
for i = 1:n
    g = 0.1*(i-1);
    [psin0,~] = groundStateBEC(u0,psi0);
    H = makeGpeHamiltonian(u0,psin0);
    var0 = variance(psin0,H);
    i
    [psin1,~] = groundStateBEC(u0,psi1);
    H = makeGpeHamiltonian(u0,psin1);
    var1 = variance(psin1,H);
    
    gList(1,i) = g;
    var0List(1,i) = var0;
    var1List(1,i) = var1;
    if(mod(i,50)==0)
        i;
    end
end

save("var1List.mat","var1List")
save("var0ListVariation.mat","var0List")
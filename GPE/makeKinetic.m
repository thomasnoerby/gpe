function T = makeKinetic()
    global nx dx;
    diagVals = ones(nx,1);
    T = -2*diag(diagVals)+diag(diagVals(1:nx-1),1)+diag(diagVals(1:nx-1),-1);
    k = -1/(2*dx^2);
    T = T*k;
end
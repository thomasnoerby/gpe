function densityPlot(runNr,Tnumber,seed)
    % run main.m until line 78 (psiInit defined)
    close all;
    global nt dt x;

    mainFolder = pwd;
    
    seedFolder = strcat("Runs/run",num2str(runNr),"/T_",num2str(Tnumber),"/seed",num2str(seed));
    cd(seedFolder)
    
    u = load("uOpt.mat").uOpt;
    T = load("T.mat").T;    
    
    cd(mainFolder)
    
    nt = floor(T/dt)+1;
    t = linspace(0, T, nt);
    
    % calculate plot values
    [psiList,FList] = forwardPropagationFList(u);
    
    xDensity = psiList.*conj(psiList);
    VDensity = potentialDensity(u,psiList);
    xExpec = calcExpecVal(x,psiList);
    
    
    % figures
    ha = tight_subplot(1,1,[0.01 .03],[.15 .1], [.15 .1]);
    set(ha(1:1),'xlim',[min(t) max(t)])
    
    % color maps
    myColorMap = hot(256);
    myColorMap(1:50,:) = 1; % removes background
    colormap(myColorMap);
    % colorbar; % put this if relevant
    
    % plot
    axes(ha(1))
    hold on
    yyaxis left
    ha(1).YAxis(1).Color = 'r';
    
    imagesc(t,x,xDensity,'AlphaData',0.5)
    ylabel('$x$')
    
    plot(t,xExpec,'r--','DisplayName','$\langle x(t) \rangle$')
    legend('Location','NW');
    
    plot(t,u,'r-','DisplayName','$u(t)$')
    
    yyaxis right;
    ha(1).YAxis(2).Color = 'b';
    FList = 1-FList;
    semilogy(t,FList,'b','DisplayName','$1-F(t)$')
    xlabel('$t$')
    
    xlim([min(t), max(t)])
    set(gca,'yscale','log')
end
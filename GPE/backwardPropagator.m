function chi = backwardPropagator(u,chi,psiToFrom)
    global UtBackward;
    
    uFrom = u(1);
    uTo = u(2);
    
    psiFrom = psiToFrom(:,1);
    psiTo = psiToFrom(:,2);
    
    chi = chiSplitStep(chi,psiFrom,uFrom);

%     z = overlap(chi,chi);
    
    chi = fft(chi);
    chi = UtBackward.*chi;
    chi = ifft(chi);
    
%     z2 = overlap(chi,chi);
%     disp(strcat("Current diff: ",num2str(z2-z),". Current ratio: ", num2str(z/z2)))
    
    chi = chiSplitStep(chi,psiTo,uTo);
end
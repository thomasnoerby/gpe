function [psi, E] = groundStateBEC(u,psi)
    % u: control value / placement 
    % psi: Initial guess
    global dx dt;

    HGpe = makeGpeHamiltonian(u,psi);
    varH = variance(psi,HGpe); % check variance
    
    % loop conditions
    i = 1; 
    iMax = 12000; % max iterations
    varThresh = 1e-5; % max variance

    while abs(varH)>varThresh && i<iMax
        HGpe = makeGpeHamiltonian(u,psi); % update Hamiltonian
        psi = expm(-dt*HGpe)*psi; % imaginary time evolution
        psi = psi/norm(psi)/sqrt(dx); % renormalization

        varH = variance(psi,HGpe); % updating variance
        i = i+1; % updating iteration number
    end
    if(i==iMax && abs(varH)>varThresh)
        disp('Maximum iterations exceeded. Variance too high; state not obtained')
    end
    
    E = overlap(psi,HGpe*psi);
end


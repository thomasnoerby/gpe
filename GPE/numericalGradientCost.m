function grad = numericalGradientCost(u)
    global nt;
    
    eps_diff = eps^(1/3);
    
    ej = zeros(1,nt);
    grad = zeros(1,nt);
    
    for i = 1:nt
        ej(1,i) = eps_diff;
        
        costPlus = costFunction(u+ej);
        costMinus = costFunction(u-ej);
        
        costDiff = costPlus-costMinus;
        
        grad(1,i) = -costDiff/(2*eps_diff);
        
        ej(1,i) = 0;
    end
end
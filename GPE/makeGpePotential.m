function VGpe = makeGpePotential(u,psi)
    global g;
    V = makePotential(u);

%     psiSquared = psi.*conj(psi);
    psiSquared = abs(psi).*abs(psi);
    VGpe = V+g*psiSquared;
end
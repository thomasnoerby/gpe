%%% Plots the first and ground states of GPE - must be run from ..

clear all; close all; clc;

addpath ..;

global nx dx x dt vDepth sigma g;

% time definitions
t0 = 0; % initial time
T = 1; % final time
dt = 5e-3; % length of a time step
nt = floor(T/dt)+1; % number of time slices
t = linspace(t0, T, nt); % time interval

% spatial definitions
xMin = -3;
xMax = 3;
L = xMax-xMin;
nx = 512; % number of x-steps
x = linspace(xMin, xMax, nx)'; % x interval

dx = (x(2)-x(1)); % length of an x-step

% momentum definitions
kVec = 2*pi/L*[0:nx/2-1, -nx/2:-1];

% BEC definitions
g = 8.3; % interaction parameter

% initial potential and control function positions
A = -1; % position of initial potential
B = 1; % position of target potential
u0 = A; % initial control function position

% control function
u = A+(1-A/B)*sin(t/T*pi/2); 

% initial state
vDepth = 130; % depth of potential
sigma = 1; % width of potential

% single particle wavefunctions
V = makePotential(u0); % create potential (only for plotting)
[eigVec, ~] = diagonalize(u0); % find eigenvectors of the single particle system
eigVec = eigVec/sqrt(dx); % proper normalisation
psi0 = eigVec(:,1); % ground state
psi1 = eigVec(:,2); % first excited state
psi = psi0; % chosen state

% Ground state/first excited state plots
figure()
xlabel("$$x$$ [sim. units]")
hold on
ylabel("$$\psi$$ [sim. units]")

% top three ground state, lower three first excited state
axis([-3 1 0 1.6])
title('$$\psi_0$$')
gList = [1.8709, 3.2573, 4.7983, 6.4855, 8.3103];
% gList = [4.2935,6.063,7.9472,9.9447,12.0533];
% axis([-3 1 -1.4 1.4])
% title('$$\psi_1$$')

[~,nG] = size(gList); % number of different g's to plot

plot(x,-psi,'DisplayName',"g = 0") % g = 0 plotted as a solid line
for i = 1:nG
    g = gList(i);
    
    psi_i = groundStateBEC(u0,psi); % calculating state
    
    plot(x,-psi_i,'--','DisplayName',strcat("g = ",num2str(gList(i)))) % plotting state
end
legend show

% plotting potential on figure
yyaxis right;
ylabel("Potential [sim. units]")
plot(x,V,'HandleVisibility','off') % no legend
axis([-3 1 -130 -100]) % ground state
% axis([-3 1 -160 -100]) % first excited state
set(gcf,"Position",[387 49 1166 843])
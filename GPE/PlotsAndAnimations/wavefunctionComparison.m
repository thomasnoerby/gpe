%%% Plots the first and ground states of GPE - must be run from ..

clear all; close all; clc;

addpath ..;

global nx dx x dt vDepth sigma g;

% time definitions
t0 = 0; % initial time
T = 1; % final time
dt = 5e-3; % length of a time step
nt = floor(T/dt)+1; % number of time slices
t = linspace(t0, T, nt); % time interval

% spatial definitions
xMin = -3;
xMax = 3;
L = xMax-xMin;
nx = 512; % number of x-steps
x = linspace(xMin, xMax, nx)'; % x interval

dx = (x(2)-x(1)); % length of an x-step

% momentum definitions
kVec = 2*pi/L*[0:nx/2-1, -nx/2:-1];

% BEC definitions
g = 5; % interaction parameter

% initial potential and control function positions
A = -1; % position of initial potential
B = 1; % position of target potential
u0 = A; % initial control function position

% control function
% u = A+(1-A/B)*sin(t/T*pi/2); 

% initial state
vDepth = 130; % depth of potential
sigma = 1; % width of potential

% single particle wavefunctions
V = makePotential(u0); % create potential (only for plotting)
[eigVec, ~] = diagonalize(u0); % find eigenvectors of the single particle system
eigVec = eigVec/sqrt(dx); % proper normalisation
psi0 = eigVec(:,1); % ground state
psi1 = eigVec(:,2); % first excited state
psi = psi1; % chosen state

% Ground state/first excited state plots
figure()
xlabel("$$x$$ [sim. units]")
hold on
ylabel("$$|\psi|^2$$ [sim. units]")

% top three ground state, lower three first excited state
title('$$\psi_1, g=5$$')

psi_MATLAB = groundStateBEC(u0,psi); % calculating state
psiSq_MATLAB = psi_MATLAB.*conj(psi_MATLAB);

plot(x,psiSq_MATLAB,'k','DisplayName',"MATLAB") % g = 0 plotted as a solid line
axis([-3 1 0 1.8])
legend show

% plotting potential on figure
yyaxis right;
ylabel("Potential [sim. units]")
plot(x,V,'HandleVisibility','off') % no legend
axis([-3 1 -130 -100]) % ground state
% axis([-3 1 -160 -100]) % first excited state


%%% compare with Composer 

data = readmatrix("psi1.csv");
x = data(:,1);
V = data(:,2);
psiSq = data(:,3);

psiSqMin = min(psiSq);

yyaxis left;

plot(x,psiSq-psiSqMin,'g--','DisplayName','Composer')

set(gcf,"Position",[387 49 1166 843])
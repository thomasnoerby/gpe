%%% This file shows an animation of the imaginary time evolution resulting
%%% in the ground and first excited states of the GPE
%%% Must be run from ..


clear all; close all; clc;

global nx dx x dt vDepth sigma;

% time definitions
t0 = 0; % initial time
T = 1; % final time
dt = 5e-3; % length of a time step
nt = floor(T/dt)+1; % number of time slices
t = linspace(t0, T, nt); % time interval

% spatial definitions
xMin = -3;
xMax = 3;
L = xMax-xMin;
nx = 512; % number of x-steps
x = linspace(xMin, xMax, nx)'; % x interval

dx = (x(2)-x(1)); % length of an x-step

% momentum definitions
kVec = 2*pi/L*[0:nx/2-1, -nx/2:-1];

% BEC definitions
g = 8.3; % interaction parameter

% initial potential and control function positions
A = -1; % position of initial potential
B = 1; % position of target potential
u0 = A; % initial control function position

% control function
u = A+(1-A/B)*sin(t/T*pi/2); 

% initial state
vDepth = 130; % depth of potential
sigma = 1; % width of potential

% single particle wavefunctions
V = makePotential(u0); % create potential (only for plotting)
[eigVec, ~] = diagonalize(u0); % find eigenvectors of the single particle system
eigVec = eigVec/sqrt(dx); % proper normalisation
psi0 = eigVec(:,1); % ground state
psi1 = eigVec(:,2); % first excited state
psi = psi0; % chosen state %%% Starting point of the animation

%%% Plot preparation - animation of imaginary time evolution
VGpe = makeGpePotential(g,u0,psi0); % Gross-Pitaevskii potential
scale = 60; % scale for wavefunction (so it is visible)

figure()
plot(x,V,'--') % 1p potential
hold on
set(gcf,'Position',[384 52 1159 870]);
h = plot(x,VGpe); % GPE potential
plot(x,conj(psi).*psi*scale-130,'--') % single particle wavefunc. dens. - starting point
f = plot(x,conj(psi).*psi*scale-130); % GPE wavefunc (or what becomes it)

HGpe = makeGpeHamiltonian(g,u0,psi); % GPE Hamiltonian
varH = variance(psi,HGpe); % variance check

% loop values
g = linspace(0,30,100);
[~,n] = size(g);
% F = [];
for i = 1:n
    psi = groundStateBEC(g(i),u0,psi0);
    VGpe = makeGpePotential(g(i),u0,psi);
    Vmin = min(VGpe);

    set(f,'YData',conj(psi).*psi*scale+Vmin) % updating wavefunction dens.
    set(h,'YData',VGpe) % updating GPE potential
%     pause(0.001)
    F(i) = getframe(gcf);
end

% I took the below from the internet
  % create the video writer with 1 fps
  writerObj = VideoWriter('myVideo.avi');
  writerObj.FrameRate = 20;
  % set the seconds per image
% open the video writer
open(writerObj);
% write the frames to the video
for i=1:length(F)
    % convert the image to a frame
    frame = F(i) ;    
    writeVideo(writerObj, frame);
end
% close the writer object
close(writerObj);
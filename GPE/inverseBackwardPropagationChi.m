function chiList = inverseBackwardPropagationChi(u,chi0,psiList)
    global nt;
    
    chi = chi0;
    
    [n,~] = size(chi);
    
    chiList = zeros(n,nt);
    chiList(:,1) = chi;
    
    for i = 1:nt-1
        ui = u(i:i+1);
        psi = psiList(:,i);
        chi = inverseBackwardPropagator(ui,chi,psi);
        chiList(:,i+1) = chi;
    end
end
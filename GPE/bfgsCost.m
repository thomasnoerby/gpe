function [F,u] = bfgsCost(u0)
    costThresh = 0.005;
    maxFuncEval = 1000;
    maxIter = 10;
    
    opt = optimoptions(@fminunc,'Display','off','ObjectiveLimit',costThresh,...
        'MaxFunctionEvaluations',maxFuncEval, ...
        'MaxIterations',maxIter);
    
    % setting problem
    problem.options = opt;
    problem.x0 = u0;
    problem.objective = @bfgsFuncWithGradCost;
    problem.solver = 'fminunc';
    
    [u,F,~,output] = fminunc(problem);
    
    F = -F; % should be cost
       
end
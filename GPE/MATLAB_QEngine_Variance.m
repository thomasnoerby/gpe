purge;

load("gList.mat");
load("var0ListVariation.mat");
load("var1List.mat");
load("../../QEngineResults/variance.mat")

% figure()
% plot(gList)
% hold on
% plot(g','--')
% title("$$g$$ comparison")

figure()
title("Variance comparison, $$\psi_0$$")
hold on
xlabel("$$g$$")
ylabel("$$var_{MATLAB}$$")
plot(gList,var0List,'k','DisplayName','MATLAB')
ylim([4e-10, 10e-10])
yyaxis right;
ylabel("$$var_{QEngine}$$")
plot(g,variance0,'.','DisplayName','QEngine')
set(gcf,'Position',[382 49 1176 836])
legend show

figure()
title("Variance comparison, $$\psi_1$$")
hold on
plot(gList,var1List,'k','DisplayName','MATLAB')
xlabel("$$g$$")
ylabel("$$var_{MATLAB}$$")
ylim([4e-10, 10e-10])
yyaxis right;
plot(g,variance1,'--','DisplayName','QEngine')
ylabel("$$var_{QEngine}$$")
set(gcf,'Position',[382 49 1176 836])
legend show


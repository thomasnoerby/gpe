function grad = analyticalGradient(u)
    global nt dt psiTar;

    [psiList,psippList] = forwardPropagation(u);
    chiList = backwardPropagation(u,psiList,psippList);
    
    grad = zeros(nt,1);
    psiT = psiList(:,end);
%     o = -1i*overlap(psiT,psiTar);
    
    for j = 1:nt
        uj = u(j);
        chi = chiList(:,j);
        psi = psiList(:,j);
        
        gradV = analyticalPotentialGradient(uj);
        
        expecVal = overlap(chi,gradV.*psi);
%         grad_j = real(-2*dt*o*expecVal);
        grad_j = real(dt*expecVal);
        grad(j,1) = grad_j;
    end
end
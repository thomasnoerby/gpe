function gradV = analyticalPotentialGradient(u)
    global x sigma p2 p4 p6;

    V = makePotential(u);
    gradV = (x-u)/sigma^2.*V;
% 
%     x_u = x-u;
%     x_uPow2 = x_u.*x_u;
%     x_uPow3 = x_u.*x_uPow2;
%     x_uPow5 = x_uPow3.*x_uPow2;
%     gradV = -p2*2*x_u-4*p4*x_uPow3-6*p6*x_uPow5;
end